<div class="jumbotron jumbotron-title">
  <div class="row">
    <div class="col-md-6">
        <h1 style="margin-bottom:15px"><img src="img/logo_subtitle_400_192.png" style="margin:0 auto;" alt="sydelity"></h1>
        <br><p style="text-align:center; color:rgba(153,0,102,1)">Applied math services for increased productivity</p>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-5">
        <div class="metro-surface">
        <table class="metro-table">
            <tr>
            <td><div id="mor-tile" class="tile tile-mor">Model<br>Reduction</div></td>
            <td><div id="regression-tile" class="tile tile-regression">Data<br>Regression</div></td>
            </tr>
            <tr>
            <td><div id="optimization-tile" class="tile tile-optimization">Optimization</div></td>
            <td><div id="contact-tile" class="tile tile-contact">Contact</div></td>
            </tr>
        </table>
        </div>
    </div>
  </div>
</div>
