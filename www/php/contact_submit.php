<?php
//add the recipient's address here
$myemail = 'info@sydelity.com';
$headers = "From: $myemail\n";
$headers .= "Reply-To: $myemail";

// validation expected data exists
if(!isset($_POST['name']) ||
	!isset($_POST['email']) ||        
	!isset($_POST['message'])) {
	died('We are sorry, but there appears to be a problem with the form you submitted.');       
	
}

//grab named inputs from html then post to #thanks
$name = $_POST['name'];
$email = strip_tags($_POST['email']);
$tags = strip_tags($_POST['tags']);
$message = strip_tags($_POST['message']);
echo "<span class=\"alert alert-success\" >Your message has been received. Thanks! Here is what you submitted:</span><br><br>";
echo "<strong>Name:</strong> ".$name."<br>";   
echo "<strong>Email:</strong> ".$email."<br>"; 
echo "<strong>Interested in:</strong> ".$tags."<br>"; 
echo "<strong>Message:</strong> ".$message."<br>";
 
//generate email and send!
$to = $myemail;
$email_subject = "Contact form submission: $name";
$email_body = "You have received a new message. ".
" Here are the details:\n Name: $name \n ".
"Email: $email\n Tags: $tags\n Message: \n $message";
$headers = "From: $myemail\n";
$headers .= "Reply-To: $email";
mail($to,$email_subject,$email_body,$headers);

?>