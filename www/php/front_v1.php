<div class="jumbotron jumbotron-title">
	<h1 style="margin-bottom:15px"><img src="img/logo_subtitle_400_192.png" style="margin:0 auto;" alt="sydelity"></h1>
    <br><p style="text-align:center; color:rgba(153,0,102,1)">Applied math services for increased productivity</p>
</div>
<style>
.metro-row{
	width:100%;
	margin:0 auto;	
}
@media screen and (min-width: 384px){
.metro-row{
	width:310px;
}}
@media screen and (min-width: 768px){
.metro-row{
	width:620px;
}}
.metro-col{
	padding:0;
}
</style>
<div class="metro-surface">
    <div class="row metro-row">
        <div class="col-xs-6 col-sm-3 metro-col">
            <div id="mor-tile" class="tile tile-mor">Model<br>Reduction</div>
        </div>
        <div class="col-xs-6 col-sm-3 metro-col">
            <div id="regression-tile" class="tile tile-regression">Data<br>Regression</div>
        </div>
        <div class="col-xs-6 col-sm-3 metro-col">
            <div id="optimization-tile" class="tile tile-optimization">Optimization</div>
        </div>
        <div class="col-xs-6 col-sm-3 metro-col">
            <div id="contact-tile" class="tile tile-contact">Contact</div>
        </div>
    </div>
</div>

