<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ('php/include.php'); ?>
   	<script async type="text/javascript" src="js/sydelity.js"></script>   
</head>

<body>
	<?php include_once("php/analyticstracking.php") ?>
	<?php $activePage="service_optimization"; ?>
	<?php include ('php/nav.php'); ?>
    <?php include('php/contact.php'); ?>
    <div class="container">
    	<div class="jumbotron jumbotron-title">
	      <div class="accordion" id="accordion1">
    	   <div class="accordion-group-border">
        	  <div class="accordion-heading accordion-heading-optimization">
            	<a id="optimization"class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#optimization-panel">
                <img src="img/optimization_header_v1.png"></a>
        </div>
        <div id="optimization-panel" class="accordion-body collapse in ">
          <div class="accordion-inner">
            <span class="label label-default label-marketing">Expensive Evaluation</span>
            <span class="label label-default label-marketing">High-dimensional</span>
            <span class="label label-default label-marketing">Global</span>
            <div class="jumbotron" style="padding-top:10px">
                <h2>Improve design quality and robustness</h2>
                <div class="marketing-slide">
                    <p>Optimal design parameters improve the yield and the reliability of your products. Sydelity incorporates novel model-based optimization techniques that ensure convergence - even for high-dimensional and expensive problems.</p>
                    <img src="img/optimization_marketing_v1.png" alt="Optimization">
                    <p>
                    With Sydelity, you can now automatically select optimal design parameters and synthesize complex layout geometries.
                    </p>
                </div>
            </div>
          </div>
        </div>
      </div>
	</div>
	</div>
      <hr>
      <footer>
        <p>© Sydelity 2014</p>
      </footer>
	</div>
<link href="css/style.css" rel="stylesheet">
</body>
</html>
