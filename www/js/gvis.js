// JavaScript Document

// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawVisualization);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawVisualization() {
	// Create and populate the data table.
	var data = google.visualization.arrayToDataTable([
		['Spec', 'ODE', 'ROM'],
		['Accuracy',  100, 98],
		['Speed', 10, 100],
		['Portability',  10, 100],
		['Security',  10, 100],
	]);
	
	var options = {
		title:"",
		width:600, height:400,
		colors: ['#AAA', '#906'],
		vAxis: {textStyle: {fontName: 'Cuprum',fontSize:24}},
		annotations: {
			textStyle: {
				fontName: 'Arial',
				fontSize: 18,
				bold: false,
				italic: true,
				color: '#871b47',     // The color of the text.
				auraColor: '#d799ae', // The color of the text outline.
				opacity: 0.8          // The transparency of the text.
			}
		}
	};
	// Create and draw the visualization.
	var chart = new google.visualization.BarChart(document.getElementById('visualization')).
		draw(data, options);
};
