	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Scaleable Virtual Prototyping">
	<meta name="keywords" content="sydelity, sidelity, model, modeling, modelling, modellering, simulation, simulate, system, model reduction, model order reduction, mor, regression, fitting, response surface model, electronic, mechanic, mechatronic, optimization, IC, integrated circuit, design process, de jonghe, dejonghe, dimitri de jonghe, alberts, andre alberts">
	<meta name="author" content="Dimitri De Jonghe">

	<title>Sydelity - Scaleable Virtual Prototyping</title>

    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<!--	<link rel="stylesheet" href="//cdn.jsdelivr.net/bootstrap.tagsinput/0.3.9/bootstrap-tagsinput.css">-->
<!--    <link href="css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="css/bootstrap-tagsinput.css" rel="stylesheet">

	<link rel="shortcut icon" href="img/logo_icon_30_24.png">
<!--    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>-->
<!--	<script async src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>-->
<!--	<script async="async" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js"></script> -->
<!--    <script src="//cdn.jsdelivr.net/bootstrap.tagsinput/0.3.9/bootstrap-tagsinput.min.js"></script>-->
	<script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script async type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script async type="text/javascript" src="js/bootstrap-tagsinput.min.js"></script>
