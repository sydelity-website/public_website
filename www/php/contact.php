<!-- Modal -->
<div class="modal fade active" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <p style="float:left; margin-right:10px;margin-top:3px"><img src="img/logo_icon_23_18.png" alt="Sydelity"></p>
        <h4 class="modal-title" id="myModalLabel">Contact us</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal contact" name="contact" id="contact-form" novalidate>
            <div class="form-group">
                <label class="control-label col-md-4" for="name">Name</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" required="required"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" for="email">Email Address</label>
                <div class="col-md-6">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" required="required"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" for="tags">Interested in</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="tags" name="tags" value="Model Reduction, Data Regression, Optimization" data-role="tagsinput" placeholder="Add tags" />
                 </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" for="message">Question or Comment</label>
                <div class="col-md-6">
                    <textarea rows="6" class="form-control" id="message" name="message" placeholder="Your question or comment here"></textarea>
                </div>
            </div>
            <div align="right">
            <input class="btn btn-primary" type="submit" value="Send" id="contact-send">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>    
