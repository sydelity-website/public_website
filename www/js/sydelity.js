// JavaScript Document
$(document).ready(function() {
	$("#mor-tile").click(function (){
		window.location = "service_mor.php";
	});	
	$("#regression-tile").click(function (){
		window.location = "service_regression.php";
	});
	$("#optimization-tile").click(function (){
		window.location = "service_optimization.php";
	});
	$("#contact-tile").click(function (){
		$('#contact-modal').modal('show'); 
	});
	
	$("#mor-link").click(function (){
		window.location = "service_mor.php";
/*		$('html, body').animate({scrollTop: $("#mor").offset().top-30}, 1000);*/
	});
	$("#regression-link").click(function (){
		window.location = "service_regression.php";
	});
	$("#optimization-link").click(function (){
		window.location = "service_optimization.php";
	});
	$("#abstaction-link").click(function (){
		window.location = "service_abstraction.php";
	});
	$("#training-link").click(function (){
		window.location = "service_training.php";
	});

	$("#contact-form").validate({
		// Specify the validation rules
		rules: {
			name: {required: true},
			email: {
				required: true,
				email: true
			}
		},
		// Specify the validation error messages
		messages: {
			name: "Please enter your name",
			email: "Please enter a valid email address",
		},
		errorPlacement: function(){
            return false;
        },
		submitHandler: function(form) {
			$.ajax({
				type: "POST",
				url: "../php/contact_submit.php", //process to mail
				data: $('form.contact').serialize(),
				success: function(msg){
					$("#contact-modal").modal('hide'); //hide popup  
				},
				error: function(){
					alert("failure");
				}
			});
		}
	});
/*	function close_toggle() {
	   if ($(window).width() <= 768) {
		  $('.nav a').on('click', function(){
			  $(".navbar-toggle").click();
		  });
	   }
	   else {
		 $('.nav a').off('click');
	   }
	}
	close_toggle();
	$(window).resize(close_toggle);*/
});