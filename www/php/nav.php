<nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
           <a class="navbar-brand" href="index.php#"><img src="img/logo_86_21_white.png" alt="logo"></a>
        </div>
        <div class="navbar-collapse collapse navbar-right">
            <ul class="nav navbar-nav">
                <li <?php if($activePage=='index'){ ?>class="active" <?php } ?>><a href="index.php#">Home</a></li>
                <li class="dropdown">
                    <a href="index.php#" class="dropdown-toggle" data-toggle="dropdown">Services <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li><a id="mor-link" style="cursor:pointer">Model Reduction</a></li>
                      <li><a id="regression-link" style="cursor:pointer">Data Regression</a></li>
                      <li><a id="optimization-link" style="cursor:pointer">Optimization</a></li>
                      <li><a id="abstaction-link" style="cursor:pointer">Abstraction</a></li>                      
                      <li><a id="training-link" style="cursor:pointer">Training</a></li>   
                    </ul>
                </li>
                <li <?php if($activePage=='about'){ ?>class="active" <?php } ?>><a href="about.php">About</a></li>
                <li><a href="index.php#contact" data-toggle="modal" data-target="#contact-modal">Contact</a></li>
            </ul>
        </div>
    </div> 
</nav>
