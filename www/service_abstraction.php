<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ('php/include.php'); ?>
   	<script async type="text/javascript" src="js/sydelity.js"></script>   
</head>

<body>
	<?php include_once("php/analyticstracking.php") ?>
	<?php $activePage="service_abstraction"; ?>
	<?php include ('php/nav.php'); ?>
    <?php include('php/contact.php'); ?>
    <div class="container">
    	<div class="jumbotron jumbotron-title">
	      <div class="accordion" id="accordion1">
    	   <div class="accordion-group-border">
        	  <div class="accordion-heading accordion-heading-optimization">
            	<a id="optimization"class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#abstraction-panel">
                <img src="img/abstraction_header_v1.png"></a>
        </div>
        <div id="abstraction-panel" class="accordion-body collapse in ">
          <div class="accordion-inner">
            <span class="label label-default label-marketing">Transistor-level</span>
            <span class="label label-default label-marketing">Behavioural</span>
            <span class="label label-default label-marketing">Functional</span>
            <span class="label label-default label-marketing">Analogue conservative</span>
            <span class="label label-default label-marketing">Discrete-time</span>
            <span class="label label-default label-marketing">Event-driven</span>
            <div class="jumbotron" style="padding-top:10px">
                <h2>Broaden your scope, accellerate, focus on essencial traits</h2>
                <div class="marketing-slide">
                    <p>
                       Capture the essential behaviour of real-world systems in a compact format. 
                       Focus on apparent behaviour, drop implementation details. 
                       Generate descriptions for various models of computation.
                       Realize speed ups in excess of 3 orders of magnitude.
                    </p>
<!--                    <img src="img/optimization_marketing_v1.png" alt="Optimization"> -->
                    <p>
                      Sydelity offers a data-driven step-by-step transformation flow allowing to move your scope from the components to the system-level.
                    </p>
                </div>
            </div>
          </div>
        </div>
      </div>
	</div>
	</div>
      <hr>
      <footer>
        <p>© Sydelity 2019</p>
      </footer>
	</div>
<link href="css/style.css" rel="stylesheet">
</body>
</html>
