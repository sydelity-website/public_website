<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ('php/include.php'); ?>
   	<script async type="text/javascript" src="js/sydelity.js"></script>   
</head>
php/
<body>
	<?php include_once("php/analyticstracking.php") ?>
	<?php $activePage="service_regression"; ?>
	<?php include ('php/nav.php'); ?>
    <?php include('php/contact.php'); ?>
    <div class="container">
    	<div class="jumbotron jumbotron-title">
      <div class="accordion" id="accordion1">
       <div class="accordion-group">
          <div class="accordion-heading accordion-heading-regression">
            <a id="regression" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#regression-panel"><img src="img/regression_header_v1.png"></a>
        </div>
        <div id="regression-panel" class="accordion-body collapse in">
          <div class="accordion-inner">
            <span class="label label-default label-marketing">System Identification - SYSID</span>
            <span class="label label-default label-marketing">High-dimensional</span>
            <span class="label label-default label-marketing">Active Learning</span>
            <span class="label label-default label-marketing">Design-of-Experiments - DoE</span>
            <span class="label label-default label-marketing">Regularization</span>
                
            <div class="jumbotron" style="padding-top:10px">
                <h2>Identify and predict.</h2>
                <div class="marketing-slide">
                    <p>Data-driven models enrich your simulation environment with real-life measurements. </p>
                    <p>
                    Sydelity offers algorithms and expert insight in appropriate response surface modeling and system identification methods. Our know-how encompasses high-dimensional data, stochastic variables and nonlinear dynamic systems.
                    </p>
                    <img src="img/regression_marketing_v1.png" alt="Data Regression">
                    <p>The regression algorithms employed by Sydelity include symbolic regression, support vector machines, neural networks, piecewise models, vector fitting, subspace identification, polynomial chaos, various sampling techniques and active learning. We customize our know-how to your modeling needs.</p> 
                </div>
            </div>
          </div>
        </div>
      </div>
       </div>
       </div>

      <hr>
      <footer>
        <p>© Sydelity 2014</p>
      </footer>
	</div>
<link href="css/style.css" rel="stylesheet">
</body>
</html>
