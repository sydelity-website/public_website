<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ('php/include.php'); ?>
   	<script async type="text/javascript" src="js/sydelity.js"></script>   
</head>

<body>
	<?php include_once("php/analyticstracking.php") ?>
	<?php $activePage="about"; ?>
	<?php include ('php/nav.php'); ?>
    <?php include('php/contact.php'); ?>
    <div class="container">
    	<div class="jumbotron jumbotron-title">
            <div class="panel panel-default">
            	<div class="panel-heading">
                	<h2>About Sydelity</h2>
                </div>
                <div class="panel-body">
                	<div class="row">
                    <div class="col-md-3">
            	    <div id="logo-tile" class="tile tile-logo">Sydelity</div>
                    </div>
                    <div class="col-md-9 about-text">
                    	<p>
	                    Sydelity is a service company specialized in modeling and optimization of complex systems.
                        </p>
    	                <p>
                        More information will follow soon...
                        </p>
                    </div>
                    </div>
                </div>
            </div>
        </div>
      <hr>
      <footer>
        <p>© Sydelity 2014</p>
      </footer>
	</div>
<link href="css/style.css" rel="stylesheet">
</body>
</html>

