<style>

/* CUSTOMIZE THE CAROUSEL
-------------------------------------------------- */

/* Carousel base class */
.carousel {
  margin-bottom: 0px;
}
/* Since positioning the image, we need to help out the caption */
.carousel-caption {
  z-index: 10;
  text-shadow: none;
}

/* Declare heights because of positioning of img element */
.carousel .item {
  height: 330px;
  background-color: transparent;
}
@media (min-width: 422px) {
.carousel .item {
  height: 400px;
  background-color: transparent;
}}


.carousel-indicators .active {
	background-color: #906;
}

.carousel-indicators li {
	border: 1px solid #906;
}

.carousel-control.left, .carousel-control.right {
	background-image: none;
}

.carousel-control:hover, .carousel-control:focus {
	color: rgba(153, 0, 102, 1);
}

.carousel-img{
	margin:0 auto;
}
@media (min-width: 1200px) {
.carousel-img{
	max-width:564px;		
}}

.carousel-title{
	text-align:left; 
	color: #906;
	font-size: 180%;
	margin-bottom:15px;
}

@media (min-width: 422px) {
.carousel-title{
	font-size: 200%;
}
}

.logo-underscript{
	text-align:center; 
	color: #906;
	font-size: 120%;
}

@media (min-width: 422px) {
.logo-underscript{
	font-size: 160%;
}}
</style>

<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>
  <div class="carousel-inner">
    <div class="item active">
	  <div class="container">
        <div class="carousel-caption">
	<h1 style="margin-bottom:15px"><img src="img/logo_subtitle_400_192.png" style="margin:0 auto;" alt="sydelity"></h1>
    <br><p class="logo-underscript">Applied math services for increased productivity</p>
		</div>
      </div>
    </div>
    <div class="item">
      <div class="container">
        <div class="carousel-caption">
		<div class="tile tile-mor tile-heading">  </div><p class="carousel-title">Model Reduction</p>
		<img src="img/mor_marketing_v5.png" class="carousel-img" alt="sydelity">
        <br>
        <p><a class="btn btn-large btn-primary" href="service_mor.php">Learn More</a></p>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="container">
        <div class="carousel-caption">
        <div class="tile tile-heading tile-regression"></div>
        <p class="carousel-title">Data Regression</p>
		<h1 style="margin-bottom:15px"><img src="img/regression_marketing_v1.png" style="margin:0 auto;" alt="sydelity"></h1>
    	<br>
        <p><a class="btn btn-large btn-primary" href="service_regression.php">Learn More</a></p>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="container">
        <div class="carousel-caption">
        <div class="tile tile-heading tile-optimization">  </div>
        <p class="carousel-title">Optimization</p>
		<h1 style="margin-bottom:15px"><img src="img/optimization_marketing_v1.png" style="margin:0 auto;" alt="sydelity"></h1>
    	<br>
        <p><a class="btn btn-large btn-primary" href="service_optimization.php">Learn More</a></p>
        </div>
      </div>
    </div>
  </div>
  <!-- Controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>  
</div>
<!-- /.carousel -->

<style>
.metro-row{
	width:100%;
	margin:0 auto;	
}
@media screen and (min-width: 384px){
.metro-row{
	width:310px;
}}
@media screen and (min-width: 768px){
.metro-row{
	width:620px;
}}
.metro-col{
	padding:0;
}
</style>
<div class="metro-surface">
    <div class="row metro-row">
        <div class="col-xs-6 col-sm-3 metro-col">
            <div id="mor-tile" class="tile tile-mor">Model<br>Reduction</div>
        </div>
        <div class="col-xs-6 col-sm-3 metro-col">
            <div id="regression-tile" class="tile tile-regression">Data<br>Regression</div>
        </div>
        <div class="col-xs-6 col-sm-3 metro-col">
            <div id="optimization-tile" class="tile tile-optimization">Optimization</div>
        </div>
        <div class="col-xs-6 col-sm-3 metro-col">
            <div id="contact-tile" class="tile tile-contact">Contact</div>
        </div>
    </div>
</div>

