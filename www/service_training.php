<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ('php/include.php'); ?>
   	<script async type="text/javascript" src="js/sydelity.js"></script>   
</head>

<body>
	<?php include_once("php/analyticstracking.php") ?>
	<?php $activePage="service_training"; ?>
	<?php include ('php/nav.php'); ?>
    <?php include('php/contact.php'); ?>
    <div class="container">
    	<div class="jumbotron jumbotron-title">
	      <div class="accordion" id="accordion1">
    	   <div class="accordion-group-border">
        	  <div class="accordion-heading accordion-heading-optimization">
            	<a id="optimization"class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#training-panel">
                <img src="img/training_header_v1.png"></a>
        </div>
        <div id="training-panel" class="accordion-body collapse in ">
          <div class="accordion-inner">
            <span class="label label-default label-marketing">verilogA</span>
            <span class="label label-default label-marketing">VHDL-AMS</span>
            <span class="label label-default label-marketing">Modelica</span>
            <span class="label label-default label-marketing">VHDL</span>
            <span class="label label-default label-marketing">SystemVerilog</span>
            <span class="label label-default label-marketing">C++</span>
            <div class="jumbotron" style="padding-top:10px">
                <h2>Get kick-started in advanced modeling techniques</h2>
                <div class="marketing-slide">
                    <p>
                      Sydelity offers trainings in advanced modelling techniques for system-level simulations.
                      Learn how earn the benefits of abstraction along the structural, time and signal axes.
                      Move your descriptions from the conservative continuous-time to the event-driven domain, 
                      whilst maintaining clean interfaces and ensuring reusability in multiple contexts.
                    </p>
                    <p>
                      In particular, Sydelity offers trainings on the following topics :
                    </p>
                    <ul>
                      <li> Wave-form Relaxation : as a means to break free from the global solver paradigm and enter a distributed world.</li>
                      <li> Event-driven Analogue : go beyond discrete-time in your WReal models.</li>
                    </ul>
                    <p>Contact us for more details.</p>
                </div>
            </div>
          </div>
        </div>
      </div>
	</div>
	</div>
      <hr>
      <footer>
        <p>© Sydelity 2019</p>
      </footer>
	</div>
<link href="css/style.css" rel="stylesheet">
</body>
</html>
